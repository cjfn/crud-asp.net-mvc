﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mvcbd.Models;

namespace mvcbd.Controllers
{
    public class CrudUsuarioController : Controller
    {
        private bd_escuelaEntities db = new bd_escuelaEntities();

        //
        // GET: /CrudUsuario/

        public ActionResult Index()
        {
            var usuario = db.usuario.Include(u => u.cargo);
            return View(usuario.ToList());
        }

        //
        // GET: /CrudUsuario/Details/5

        public ActionResult Details(int id = 0)
        {
            usuario usuario = db.usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        //
        // GET: /CrudUsuario/Create

        public ActionResult Create()
        {
            ViewBag.car_cod = new SelectList(db.cargo, "car_cog", "car_des");
            return View();
        }

        //
        // POST: /CrudUsuario/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.usuario.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.car_cod = new SelectList(db.cargo, "car_cog", "car_des", usuario.car_cod);
            return View(usuario);
        }

        //
        // GET: /CrudUsuario/Edit/5

        public ActionResult Edit(int id = 0)
        {
            usuario usuario = db.usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.car_cod = new SelectList(db.cargo, "car_cog", "car_des", usuario.car_cod);
            return View(usuario);
        }

        //
        // POST: /CrudUsuario/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.car_cod = new SelectList(db.cargo, "car_cog", "car_des", usuario.car_cod);
            return View(usuario);
        }

        //
        // GET: /CrudUsuario/Delete/5

        public ActionResult Delete(int id = 0)
        {
            usuario usuario = db.usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        //
        // POST: /CrudUsuario/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            usuario usuario = db.usuario.Find(id);
            db.usuario.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}